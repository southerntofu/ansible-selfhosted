# tor

This service role configures a certain number of onion services vhosts.

## Arguments

This role will configure some vhosts, defined in:

- `vhosts` variable, when called by another role
- `tor.vhosts` variable, when not called by another role (`vhosts` is empty)

A vhost consists of the following arguments:

- **(required)** `name`: an identifier for the onion vhost, usually a qualified domain name
- **(required)** `ports`: a mapping of ports to proxy to a certain address/port 

## Settings

No settings yet.

## Conventions

In order to interface with other roles/services, this role follows a certain number of conventions:

- onion directories are placed in `/var/lib/tor/IDENTIFIER`, where `IDENTIFIER` is the vhost's name
- `/var/lib/tor/IDENTIFIER/hostname` contains the generated onion domain

## TODO

Maybe we should support onion balancing?
