# chatbridge

This role configures a certain number of chatbots to relay messages across the following networks:

- [x] Jabber/XMPP
- [x] IRC
- [ ] matrix
- [ ] a lot more supported by [matterbridge](https://github.com/42wim/matterbridge), the software used under the hood ; some are disabled in the build options because compiling everything [takes too much RAM](https://github.com/42wim/matterbridge/issues/497) ([list of disabled networks in "Setup matterbridge" task](tasks/main.yml))

## Arguments

This role will configure some chans, defined in `chatbridge.chans` variable. Each chan consists of the following arguments:

- **(required)** `name`: a unique identifier for this chan
- **(required)** `chans`: a mapping from account name to a channel name

[Chan profiles](../README.md#profiles) can also be defined, as explained in the [Profiles](#Profiles) section. Account configuration is described in the [Settings](#settings) section.

## Profiles

All top-level configuration keys in `chatbridge` that are neither `chans` nor `settings` refer to a certain chan profile defined in `chatbridge.settings.profiles`. Each of those profile entries contains a list of channel names to apply through the profile.

The settings of the chan profile (which accounts/servers it applies to, and with which naming scheme) are defined in the profile settings, as explained in the [Settings](#settings) section. Here's an example configuration from joinjabber.org:

```
chatbridge:
  (...)
  joinjabber: # Profile name
    - sysadmin
    - bridging
    - abuse
    - privacy
    - website
    - translations
settings:
  (...)
  profiles:
    joinjabber: # Profile name
      joinjabber: "$room" # Naming scheme for joinjabber server
      tildechat: "#joinjabber-$room" # Naming scheme for tildechat server
```

## Settings

The `chatbridge` role settings are defined in the configuration file, under the `chatbridge.settings` key. The following settings are available:

- **(required)** `accounts`: a list of accounts, as explained below
- `nick`: the bot's nick, currently only used by XMPP (default: `relay`)
- `format`: the message format (default: `[{PROTOCOL}] <{NICK}>`)
- `profiles`: a dictionary of profile names with matching configuration

Each account must have the following **required** settings:

- `name`: a unique identifier for this account, as used by chan declarations
- `type`: the protocol used by this account  (`irc` or `xmpp`)
- `login`: the username to connect with to the server ; for XMPP accounts, the client's server is determined by this variable
- `server`: the hostname of the server to connect to ; for XMPP, this refers to the MUC server for chatrooms, and the client's server is configured in `login` setting

For example, take the joinjabber.org accounts config:

```
  settings:
    accounts:
      - name: "joinjabber"
        type: "xmpp"
        login: "jjbot@jabber.fr"
        server: "joinjabber.org"
      - name: "tildechat"
        type: "irc"
        login: "relay"
        server: "irc.tilde.chat"
```

**Note that passwords are not stored in the configuration file (which is meant to be shared), as explained in the [Conventions section](#conventions)**

Each profile must have as configuration a dictionary mapping an account name to a naming scheme containing `$room`, which will be substituted with the room name, for example:

```
chatbridge:
  settings:
    profiles:
      mychatrooms:
        myxmppserver: "$room"
        liberachat: "#myown-$room"
        tildechat: "#$room-mine"
```

## Conventions

In order to interface with other roles/services, this role follows a certain number of conventions:

- passwords are stored plaintext in `/opt/chatbridge/passwords/` folder, in a file named after the account name
- for XMPP accounts, the actual credentials are stored in `/opt/chatbridge/passwords/xmpp_JID`, where `JID` is the actual login (multiple MUC "accounts" can use the same login)

## TODO

- [ ] Support [multiple systemd profiles](https://www.stevenrombauts.be/2019/01/run-multiple-instances-of-the-same-systemd-unit/) to run parallel instances of matterbridge
- [ ] Support puppet mode for jabberserver
- [ ] To avoid collisions, gateway name should probably be "PROTOCOL.NAME" instead of just "NAME" ; this would also be applied to credentials paths
