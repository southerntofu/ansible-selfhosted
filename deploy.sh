#!/bin/bash
LOGLEVEL="-vv"

# ./deploy.sh - Runs ansible playbook in roles/main.yml, applying it to the local machine
#               with variables loaded from config.yml
# ./deploy.sh [ROLE..] - Same as above, but overrides the enabled roles (services) in
#                        config.yml with those passed as positional arguments.
#                        Example: ./deploy.sh webserver mailserver

# Output valid YAML containing all args in a services list
mkvariable() {
    echo "services:"
    while [ $# -gt 0 ]; do
        ADD=1
        echo "  - \"${1}\""
        # Drop one argument
        shift
    done
}

if [ $# -gt 0 ]; then
    # Generate temporary file which will override config.yml, because passing variables directly behaves weird
    TMPFILE=$(mktemp)
    mkvariable $@ > $TMPFILE
    ANSIBLE_RETRY_FILES_ENABLED=0 ansible-playbook -e @config.yml -e @$TMPFILE roles/main.yml $LOGLEVEL --connection=local --inventory 127.0.0.1, --limit 127.0.0.1
    rm $TMPFILE
else
    ANSIBLE_RETRY_FILES_ENABLED=0 ansible-playbook -e @config.yml roles/main.yml $LOGLEVEL --connection=local --inventory 127.0.0.1, --limit 127.0.0.1
fi
