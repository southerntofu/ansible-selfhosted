{%- if (vhost_type | default("virtualhost")) == "virtualhost" -%}
VirtualHost "{{ host }}"{%- elif (vhost_type | default("virtualhost")) == "component" -%}
Component "{{ host }}"{% else %}
Component "{{ host }}" "{{ vhost_type }}"{% endif %}

  {% if item.admins|default(None) -%}
    admins = { {% for admin in item.admins -%}"{{ admin }}"{% if not loop.last %}, {% endif %}{% endfor %} };
  {%- endif %}

  {% if item.contact|default(None) -%}
  {# When no contact_info is specified on a vhost, prosody will advertise main contact_info #}
contact_info = {
    abuse = { {% for addr in item.contact %}"{{ addr }}"{% if not loop.last %}, {% endif %}{% endfor %} };
    admin = { {% for addr in item.contact %}"{{ addr }}"{% if not loop.last %}, {% endif %}{% endfor %} };
  }
  {%- endif %}

{% if item.tls|default(true) %}
  ssl = {
    certificate = "/etc/letsencrypt/live/{{ host }}/fullchain.pem";
    key = "/etc/letsencrypt/live/{{ host }}/privkey.pem";
  }
{% endif %}

  modules_enabled = {
  {% for module in modules_enabled %}{% if not loop.first %}{{ "  " }}{% endif %}  "{{ module }}";{{ "\n" }}{%- endfor %}
}

  modules_disabled = {
  {% for module in modules_disabled %}{% if not loop.first %}{{ "  " }}{% endif %}  "{{ module }}";{{ "\n" }}{%- endfor %}
}

{% for extra in vhost_extras %}
{% set value_type = (extra.value | type_debug) %}
{% if value_type == "str" or value_type == "AnsibleUnsafeText" %}  {{ extra.key }} = "{{ extra.value }}";
{% elif value_type == "int" %}  {{ extra.key }} = {{ extra.value }};
{% elif value_type == "list" %}  {{ extra.key }} = { {{ extra.value | join("; ") }} }
{% elif value_type == "dict" %}  {{ extra.key }} = { {% for k, v in extra.value.items() %}{{ k }} = "{{ v }}";{# How deep should we go handling types? #}{% endfor %} }
{% endif %}
{%- endfor %}
