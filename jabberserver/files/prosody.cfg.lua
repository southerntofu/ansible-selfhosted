pidfile = "/run/prosody/prosody.pid";

{% if jabberserver|default(None) %}{# TODO: make it work when jabberserver is not defined or does not have settings #}
admins = { {% for admin in jabberserver.settings.admins -%}"{{ admin }}"{% if not loop.last %}, {% endif %}{% endfor %} };

{% if jabberserver.settings.contact|default(None) %}
contact_info = {
  abuse = { {% for addr in jabberserver.settings.contact %}"{{ addr }}"{% if not loop.last %}, {% endif %}{% endfor %} };
  admin = { {% for addr in jabberserver.settings.contact %}"{{ addr }}"{% if not loop.last %}, {% endif %}{% endfor %} };
}
{% else %}{# No specific jabber contact info, build from admins list and server contact info #}
{% if contact|default(None) %} {# Fallback: global server contact info #}
{% set contacts = [] %}
{% for mail in contact.email %}{{ contacts.append("mailto:" ~ mail) }}{% endfor %}
{% for muc in contact.xmpp_muc|default([]) %}{{ contacts.append("xmpp:" ~ muc ~ "?join") }}{% endfor %}
{% for jid in contact.xmpp_users|default([]) %}{{ contacts.append("xmpp:" ~ jid) }}{% endfor %}
{% for jid in jabberserver.settings.admins %}{{ contacts.append("xmpp:" ~ jid) }}{% endfor %}
contact_info = {
  abuse = { {% for addr in contacts %}"{{ addr }}"{% if not loop.last %}, {% endif %}{% endfor %} };
  admin = { {% for addr in contacts %}"{{ addr }}"{% if not loop.last %}, {% endif %}{% endfor %} };
}
{% else %}{# Fallback: contact@hostname + jabber admins #}
contact_info = {
  abuse = { "mailto:contact@{{ hostname }}"{% for admin in jabberserver.settings.admins %}, "xmpp:{{ admin }}"{% endfor %} };
  admin = { "mailto:contact@{{ hostname }}"{% for admin in jabberserver.settings.admins %}, "xmpp:{{ admin }}"{% endfor %} };
}
{% endif %}
{% endif %}
{% endif %}

plugin_paths = { "/usr/lib/prosody/prosody-modules-enabled" };

modules_enabled = {
{% for module in modules_enabled %}  "{{ module }}";{{ "\n" }}{%- endfor %}
}

modules_disabled = {
{% for module in modules_disabled %}  "{{ module }}";{{ "\n" }}{%- endfor %}
}

network_backend = "epoll";

c2s_require_encryption = true;
s2s_require_encryption = true;
s2s_secure_auth = true;

authentication = "internal_hashed"

-- Only useful when web clients are enabled, but maybe they're only enabled
-- for a specific vhost, so this should stay here
http_ports = { 5280 };
http_interfaces = { "127.0.0.1", "::1" };
https_interfaces = {};

consider_bosh_secure = true;
consider_websocket_secure = true;

muc_log_expires_after = "1w";

storage = {
  archive = "xmlarchive";
  muc_log = "xmlarchive";
}

log = {
    error = "/var/log/prosody/prosody.err";
    { levels = { "info" }; to = "syslog"; };
}

-- Required when no other vhost. It doesn't harm to be here anyway
VirtualHost "localhost"
  modules_disabled = { "c2s", "tls", "s2s" }

Include "/etc/prosody/conf.d/*.cfg.lua"
