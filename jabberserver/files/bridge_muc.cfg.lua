Component "{{ item.host }}"
  component_secret = "{{ lookup('password', '/etc/prosody/secrets/' ~ item.host ~ ' length=64 chars=ascii_lowercase') }}"

  ssl = {
    certificate = "/etc/letsencrypt/live/{{ item.host }}/fullchain.pem",
    key = "/etc/letsencrypt/live/{{ item.host }}/privkey.pem"
  }
