# jabberserver

This service role configures a certain number of vhosts for a Jabber/XMPP server. Features include:

- [x] integrated TLS support (**Note:** Server without TLS is not supported at the moment)
- [x] IPv4/IPv6 support
- [x] multiple profiles (`primary`, `muc`, `upload`, `anonymous`, `gateway_irc`)
- [x] optional anonymous login for group discussions (guest mode for `muc` profile)
- [x] optional webclient autoconfiguration for anonymous mode ([xmpp-web](https://github.com/nioc/xmpp-web), [converseJS](https://conversejs.org/)), doesn't support non-anonymous login for `primary` profile yet
- [ ] .onion federation via `mod_onions`

## Arguments

This role will configure some vhosts, defined in:

- `vhosts` variable, when called by another role
- `jabberserver.vhosts` variable, when not called by another role (`vhosts` is empty)
- [vhost profiles](../README.md#profiles) within `jabberserver` variable, when not called by another role (`vhosts` is empty) ; reserved keywords for profile names are `settings` and `vhosts`

A vhost consists of the following arguments:

- **(required)** `host`: the fully qualified domain name to serve the virutalhost with
- **(required)** `template`: the profile of vhost to configure, when not determined by a [vhost profile](../README.md#profiles) (default:`muc`)
- `admins`: a list of JIDs who have admin powers over the vhost (default: `jabberserver.settings.admins`)
- `contact`: a list of contact URIs for admins of this vhost (default: `jabberserver.settings.contact`)
- `tor`: enable or disable tor onion services (defautl: `jabberserver.settings.tor`)

Additionally, each vhost profile may support additional settings. Please refer to the [Profiles](#profiles) section for information on that. These settings can be defined on a per-vhost basis, or globally, as explained later.

**Warning:** When called from a profile shorthand, the vhost's `template` is empty, and the global variable `template` is used instead. **TODO:** Maybe we'd like to use `template` (or even `profile`) in both cases to avoid confusion?

# Profiles

[vhost profiles](../README.md#vhost-profiles) are a quick way to support different kind of use-cases from a single role. In the `jabberserver` role, some profiles are already included. They are described below, along with their specific settings:

- `muc`: setup a server for chatrooms
  - **TODO** `archive`: how long to keep messages history for (default: `jabberserver.settings.archive`)
  - **TODO** `creation`: who can create rooms, can be `admins`, `open`, or `local` to only allow members from a parent domain to create rooms (default: `jabberserver.settings.creation`)
  - `anonymous`: settings for anonymous (guest) connections to the chatrooms (**TODO**: should be passed as such to anonymous subprofile issue #14)
    - `host`: hostname to use to enable anonymous/guest accounts (default: `None`)
    - **TODO** `upload`: `upload`: `true`/`false` to enable anonymous file uploads (default: `jabberserver.settings.anonymous.upload`)
  - `default_rooms`: a list of chatrooms to suggest to clients for autojoin, used by `conversejs` web client (default: `jabberserver.settings.default_rooms`)
  - `web_client: used only when anonymous connections are enabled, and passed to configuration of related `anonymous` profile (default: `jabberserver.settings.web_client`)
- `primary`: setup a server for serving user accounts
  - `upload`: settings for HTTP upload for the server (**TODO**: should be passed as such to upload subprofile issue #14)
    - `host`: hostname to use for HTTP upload (default: `jabberserver.settings.upload.host`, or `share.HOST` otherwise)
  - `muc`: settings for a chatroom (MUC) vhost associated to the server (**TODO**: should be passed as such to muc subprofile issue #14)
    - `host`: hostname for the chatroom server (default: `jabberserver.settings.muc.host`, or `groups.HOST` otherwise)
  - `gateway_irc`: settings for the IRC gateway (**TODO:** should be passed as such to gateway_irc subprofile issue #14)
    - `host`: hostname for the irc gateway (default: `jabberserver.settings.gateway_irc.host`, or `irc.HOST` otherwise)
  - **TODO**: `login`: how to authenticate users, can be TODO `ldap`, TODO `unix`, or `local` for the jabberserver to handle its accounts separately from the rest of the system (default: `jabberserver.settings.login`)
  - **TODO**: `web_client`: configure a web client for registered users of this vhost, can be `xmpp-web` or `conversejs` (default: `jabberserver.settings.web_client`)
- `anonymous`: setup an anonymous/guest server
  - `web_client`: configure a web client for guests, can be `xmpp-web` or `conversejs` (default: `jabberserver.settings.web_client`)
  - `default_muc`: a MUC server to suggest to clients, used by `xmpp-web` web client (default: `jabberserver.settings.default_muc`)
  - `default_rooms`: a list of chatrooms to suggest to clients for autojoin, used by `conversejs` web client (default: `jabberserver.settings.default_rooms`)

**Note:** Support for web clients (via BOSH and websocket endpoints) is only enabled for a vhost if `web_client` is configured, because we do not wish to encourage users to give their credentials blindly to web services they encounter.

## Make your own profile

If you want to make your own profile, it's very easy. You simply need to create a `roles/jabberserver/tasks/templates/foo.yml` file containing Ansible instructions. If most cases, you also need to create a related template in `roles/jabberserver/files/foo.conf` file, to place in `/etc/prosody/conf.d/` folder.

## Settings

The `jabberserver` role settings are defined in the configuration file, under the `jabberserver.settings` key. The following settings are available:

- **(required)** `admins`: a list of JIDs for admins, used as fallback when a vhost doesn't have its own admins
- `contact`: a list of default contact URIs for jabber admins; can be overriden by a vhost's `contact` settings (default: mix `jabberserver.settings.admins` and global `contact`, or `admins` and contact@`hostname` if there is no top-level contact config)
- `tor`: enable or disable tor by default for vhosts (default: `true`)
- `muc`: default chatroom settings (MUC), unless overridden in a vhost
  - `host`: default chatroom server, unless overriden in a vhost (default: `None`, in which case `groups.HOST` may be used) 
- `upload`: default HTTP upload settings
  - `host`: default HTTP upload server, unless overridden in a vhost (default: `None`, in which case `share.HOST` may be used)
- `gateway_irc`: default IRC gateway server, unless overridden in a vhost (default: `None`, in which case `irc.HOST` may be used)
- TODO `anonymous`: default anonymous login settings
  - `host`: default anonymous login server, unless overriden in a vhost (default: `None`, in wihch case `guests.HOST` may be used)
  - TODO `upload`: enable anonymous file uploads, when anonymous login is enabled (default: `false`)
- TODO `archive`: how long to keep messages history for (default: `1w`)
- TODO `creation`: who can create chatrooms (default: `local`)
- `web_client`: whether to enable a web client (default: `None`)
- TODO `login`: how users are identified (default: `local`)
- TODO `ldap_settings`: global LDAP settings (default: `None`)
- `default_muc`: a MUC server to suggest to clients, used by `xmpp-web` web client (default: `None`)
- `default_rooms` a list of chatrooms to suggest to clients for autojoin, used by `conversejs` web client (default: `None`)

## Conventions

In order to interface with other roles/services, this role follows a certain number of conventions:

- TLS certificate is generated by a [tls role following a documented interface](../tls/README.md) ; generated certificates are found in `/etc/letsencrypt/live/HOSTNAME/{fullchain,privkey}.pem`
- webroot of the vhost for web client is always `/var/www/HOSTNAME/`

## TODO

Apart from the TODOs listed here, what would be nice:

- .onion federation, including tricky unspecified .onion<->DNS aliases of JIDs
- Movim/Libervia integration for "social" Jabber/XMPP (PubSub profile)
- Jitsi Meet integration (maybe subprofile?)
- biboumi should be able to run different instances with different settings, eg. public instance to reach a specific IRC network, or local instance for local users to reach any IRC network, and/or a second dedicated biboumi for bridging, so that actual users using a transport and the bridge aren't on the same domain
- templates should support vhost-level admins (item.admins)
- templates should support restrict_s2s settings, which can be either `true` (prevent ANY s2s, even local), `false` (default, allow ANY s2s), `local` (only allow localhost s2s with vhosts known from prosody), or a list of allowlisted domains (eg. `[ "bridge.fr.tild3.org" ]`)
- cheogram-muc-bridge integration as part of MUC profile:
  - when upstream supports mapping entire MUC server to an IRC namespace, with per-chan configuration overrides possible
  - declare bridge@BRIDGEHOST (that's a constant) JID admin to the MUC server, maybe... maybe there's a way to access JIDs on MUC side without being admin? when not tied to a specific trusted MUC server, hashing rules for suffixes apply for security concerns
