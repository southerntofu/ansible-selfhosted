---

- name: "Install prosody"
  ansible.builtin.apt:
    default_release: "bullseye-backports"
    name:
      - "prosody"
      - "telnet"
      - "mercurial"
    state: "latest"
  notify: "restart prosody"

- name: "Check prosody-modules already exist locally"
  ansible.builtin.stat:
    path: "/var/lib/prosody/prosody-modules"
  register: modules

- name: "Fetch prosody-modules"
  community.general.hg:
    repo: "https://hg.prosody.im/prosody-modules"
    dest: "/var/lib/prosody/prosody-modules"
    clone: true
    purge: true
  register: fetch
  failed_when: not modules.stat.exists and fetch.failed

- name: "Create prosody modules folder tree"
  ansible.builtin.file:
    path: "/usr/lib/prosody/prosody-modules-enabled"
    state: "directory"

- name: "Create vhost configuration folder for prosody"
  ansible.builtin.file:
    path: "/etc/prosody/conf.d"
    state: "directory"

- name: "Add prosody to tls group"
  ansible.builtin.user:
    name: "prosody"
    append: true
    groups: "tls"

- name: "Enable community modules"
  ansible.builtin.file:
    src: "/var/lib/prosody/prosody-modules/mod_{{ item }}"
    path: "/usr/lib/prosody/prosody-modules-enabled/mod_{{ item }}"
    state: "link"
  loop:
    # TODO: - "auto_answer_disco_info"
    - "bookmarks2"
    - "cloud_notify"
    # TODO: - "http_altconnect"
    - "http_upload"
    - "rawdebug"  # Not loaded but useful to have available
    - "smacks"
    - "storage_xmlarchive"
    - "vcard_muc"
  notify: "restart prosody"

# We now use mod_bookmarks2. mod_bookmarks will conflict with the next version
# of prosody's bundled bookmark module.
- name: "Ensure mod_bookmarks from community isn't symlinked"
  ansible.builtin.file:
    path: "/usr/lib/prosody/prosody-modules-enabled/mod_bookmarks"
    state: "absent"

- name: "Processing vhosts passed by another role"
  ansible.builtin.include_tasks: "vhost.yml"
  loop: "{{ vhosts }}"
  when: vhosts is defined

- name: "Processing config vhosts"
  ansible.builtin.include_tasks: "vhost.yml"
  loop: "{{ jabberserver.vhosts }}"
  when: jabberserver.vhosts is defined and vhosts is not defined

- name: "Process shorthands"
  ansible.builtin.include_tasks: "process.yml"
  loop: "{{ jabberserver|default({}) | dict2items }}"
  when: vhosts is not defined and
    template_kind.key != "settings" and
    template_kind.key != "vhosts"
  loop_control:
    loop_var: template_kind

- name: "Configure prosody"
  ansible.builtin.template:
    src: "files/prosody.cfg.lua"
    dest: "/etc/prosody/prosody.cfg.lua"
  notify: "restart prosody"

- name: "Ensure prosody is started / enabled"
  ansible.builtin.service:
    name: "prosody"
    state: "started"
    enabled: true
