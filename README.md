# ansible-selfhosted

**WARNING:** This is a codename and will change. The recipes are alpha-quality software. They're good enough for us, but expect rough edges for a while.

This repository contains a number of recipes to make your life easier when managing servers. The idea is deploying a static website with full TLS and Tor's onion services support should be as easy as:

```
hostname: "example.com"
contact:
  email: "example@riseup.net"
services:
  - webserver
webserver:
  static:
    - host: "example.com"
      git: "https://codeberg.org/foo/bar"
```

But don't take this README's word for it! You can read the actual configuration files describing the infrastructures of:

- [fr.tild3.org](https://tildegit.org/tilde-fr/infra/src/branch/master/config.yml) (Debian buster), a self-organized community of enthusiastic french-speaking software hackers
- [joinjabber.org](https://codeberg.org/joinjabber/infra/src/branch/main/config.yml) (Debian buster), a grassroots community effort to promote user-friendly information about the Jabber/XMPP ecosystem

Our recipes are also starting to support Archlinux, and Debian bullseye should work just fine. Stay tuned for more information on that! In the meantime, you will find in this repository:

- [Ansible](https://en.wikipedia.org/wiki/Ansible_(software)) roles to implement the services
- a [deploy.sh](deploy.sh) script to apply these settings on a server (run as root)

# Setup

To configure your server, you will need a dedicated folder to keep track of all the configuration. It is recommended to version this folder with git or mercurial (or the tool of your choice). This repository should then be cloned as a submodule in the `roles` folder. Assuming you want to store server data in `/etc/server`, you could do:

```
# mkdir /etc/server; cd /etc/server
# git init
# git submodule add https://codeberg.org/southerntofu/ansible-selfhosted /etc/server/roles
```

Now, you should:

- copy the [deploy.sh](deploy.sh) script to your server's folder
- configure the server in `config.yml`
  * here's some example configs: [joinjabber.org](https://codeberg.org/joinjabber/infra/src/branch/main/config.yml) [fr.tild3.org](https://tildegit.org/tilde-fr/infra/src/branch/master/config.yml)
  * the complete configuration format is explained in this README, as well as in the README documents of individual roles linked in this document (refered to as `docs`)
- run `./deploy.sh`
- enjoy?

Your server configuration folder should look like this in the end:

- `config.yml`: the unique configuration file for your entire server
- `deploy.sh`: the script to apply the configuration
- `roles/`: the recipes describing how to apply the configuration

# Services

We currently support the following high-level services:

- **webserver** ([docs](webserver/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19761)): Web server with support for multiple virtual hosts, TLS/Tor, inner routes overrides
- **mailserver** ([docs](mailserver/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19760)): Basic email server for local mailboxes and aliases (no LDAP/PAM yet)
- **jabberserver** ([docs](jabberserver/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19759)): Jabber/XMPP server for instant messaging and social networking, with support for anonymous vhosts and web clients (no LDAP/PAM yet)
- **chatbridge** ([docs](chatbridge/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19762)): [matterbridge](https://github.com/42wim/matterbridge) bot to relay discussions across channels and networks (IRC/XMPP/Matrix)
- (alpha) **mucbridge** ([docs](mucbridge/README.md)): [cheogram-muc-bridge](https://git.singpolyma.net/cheogram-muc-bridge) for puppeting and private messages support when bridging MUCs (including gateway MUCs to other networks)
- **personal_pages_web** ([docs](personal_pages_web/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19763)): Personal pages for users with support for a personal onion `https://hostname/~user` (no LDAP yet)

Some other roles are available, especially for usage as building blocks (helpers) by higher-level roles. Some of these roles are various package managers. You will find more information about these in the [Docs](#docs) section of this document. Additionally, we'd like to support more roles in the future, for example:

- `nameserver`: setup primary/secondary DNS server
- `firewall`: let services declare their firewall policy
- `backup`: let services declare their backup procedures

If you would like to get involved and contribute some new services, please head over to our [chatroom sysadmin@joinjabber.org](xmpp:sysadmin@joinjabber.org?join).

# Configuration

The configuration file from the server is `config.yml`. In addition to specific role configuration, there's a number of high-level settings in there, explained in this section.

## Base settings

In order to operate properly, the server needs a certain number of base settings:

- `hostname`: the main domain name for the server
- `aliases`: a list of domain names also vaild for this server, which will be included in the main TLS certificate
- `contact`: contact information used by some roles, for example for abuse reports
  - `email`: a list of contact emails (default: `contact@` followed by `hostname`)
  - `xmpp_muc`: a list of Jabber/XMPP chatrooms (default: `None`)
  - `xmpp_users`: a list of Jabber/XMPP users (default: `None`)

## Packages

**TODO:** Package managers should be better documented and their interface standardized. WTF does go not accept https://github.com/ as valid repository source and require to strip the scheme?

The configuration file contains a `packages` field, which is a mapping of package managers to a list of packages for them to install. How the list of packages is treated may be different for every package manager. For example:

```
packages:
  debian: [ "tmux" ]
  rust:
    - "lsd"
    - bin: "zola"
      repo: "https://github.com/southerntofu/zola"
      version: "bugfix-index-translations"
```

This configuration snippet above will setup three packages:

- tmux from the Debian repositories
- lsd from [crates.io](https://crates.io)
- zola from [github.com/southerntofu/zola](https://github.com/southerntofu/zola) on the `bugfix-index-translations` branch

Internally, every package manager is a role prefixed by a dot, to avoid confusion with other roles (services). We currently support the following package managers:

- debian ([docs](.debian/tasks/main.yml))
- rust ([docs](.rust/tasks/main.yml))
- go ([docs](.go/tasks/main.yml))

**Note:** `.common` is a reserved role name, and cannot be used for a `common` package manager.

## Services

The configuration file has a `services` field, which is a list of services (roles) to setup on the server. In most cases, it should contain the `.common` role, which will perform the base setup. For example:

```
services:
  - ".common"
  - "webserver"
```

This snippet above will configure the base server, as well as a webserver. The webserver configuration, like any other service configuration, is kept in a separate field so the services remains human-readable. The field containing the configuration for a specific service wears the same name as the service (role) itself. For example, webserver configuration lives in a `webserver` field in the configuration file. Inside a service configuration, global settings are usually defined in a `settings` key, for example `webserver.settings`. 

## Virtualhosts

Most services can be configured to serve different domains/users. These specific configuration blocks are separated into what we call virtualhosts, or vhosts for short. Each such vhost for a service is configured in the `vhosts` list, inside the service configuration block. For example, `webserver.vhosts`. How to configure a vhost for a specific service is left to interpretation, and you should refer to service docs in order to find out what kind of settings a vhost may accept/require, and what the service will do with those settings.

**Note:** Some services may use similar concepts, though not called "vhosts". For example, `chatbridge` service uses `chans` for multiple configurations.

## Profiles {#profiles}

A service may support different use-cases for each vhost. While these are usually defined using the `template` setting of a vhost, we provide a high-level shorthand method for defining separate profiles without typing it out for each individual vhost. Each service profile is a top-level configuration key inside the service configuration, wearing the name of the service profile, and containing a list of vhosts which will be loaded with `template` set to this profile name. Take the following configuration snippet:

```
webserver:
  vhosts:
    - host: "example.org"
      template: "static"
      git: "https://git.example.org/foo/bar"
    - host: "example.net"
      template: "static"
      git: "https://git.example.net/foo/bar"
```

The above snippet is strictly equivalent to:

```
webserver:
  static:
    - host: "example.org"
      git: "https://git.example.org/foo/bar"
    - host: "example.net"
      git: "https://git.example.net/foo/bar"
```

Additionally, some services support subprofiles to let you configure **parts** of a vhost more specifically. For example, you can use webserver subprofiles (as explained in webserver docs) to configure specific routes within an existing vhost.

# Deploying

A [deploy.sh](deploy.sh) script is provided to make deploying easier. It's intended to be run as root from the server itself, and optionally accepts positional arguments representing a list of services to (re)configure, instead of following the services list defined in the configuration file:

```
# # Setup the server according to config.yml
# ./deploy.sh
# # Only reconfigure webserver
# ./deploy.sh webserver
```

# Documentation

This document is the top-level documentation. In addition, you may find documentation about:

- services:
  - **webserver** ([docs](webserver/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19761)): Web server with support for multiple virtual hosts, TLS/Tor, inner routes overrides
  - **mailserver** ([docs](mailserver/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19760)): Basic email server for local mailboxes and aliases (no LDAP/PAM yet)
  - **jabberserver** ([docs](jabberserver/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19759)): Jabber/XMPP server for instant messaging and social networking, with support for anonymous vhosts and web clients (no LDAP/PAM yet)
  - **chatbridge** ([docs](chatbridge/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19762)): [matterbridge](https://github.com/42wim/matterbridge) bot to relay discussions across channels and networks (IRC/XMPP/Matrix)
  - (alpha) **mucbridge** ([docs](mucbridge/README.md)): [cheogram-muc-bridge](https://git.singpolyma.net/cheogram-muc-bridge) for puppeting and private messages support when bridging MUCs (including gateway MUCs to other networks)
  - **personal_pages_web** ([docs](personal_pages_web/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19763)): Personal pages for users with support for a personal onion `https://hostname/~user` (no LDAP yet)
- helpers:
  - **tls** ([docs](tls/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19765)): Generate [TLS certificates](https://en.wikipedia.org/wiki/Transport_Layer_Security) (eg. for HTTPS) from [Let's Encrypt](https://letsencrypt.org/) or other ACME servers (soon) for a domain and some potential aliases
  - **tor** ([docs](tor/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19766)): Generate [Tor](https://torproject.org/) [onion services](https://community.torproject.org/onion-services/) and manage port mappings (reverse proxy) for each onion
  - **peering** (**TODO** [docs](peering/README.md), [todo](https://codeberg.org/southerntofu/ansible-selfhosted/issues?labels=19764)): Interact securely with other servers (for backup or secondary services) by exchanging public SSH keys (functional but unused as of 2021-07-01)
- user management
  - **unix_users** (**TODO** [docs](unix_users/README.md)): Set UNIX user accounts on the server, with public-key SSH authentication
  - TODO: **ldap_users**
- package managers ([todo](https://codeberg.orG/southerntofu/ansible-selfhosted/issues?labels=19767)):
  - rust (**TODO** [docs](.rust/README.md))
  - go (**TODO** [docs](.go/README.md))
- distro-specific package managers:
  - Debian: `debian` (**TODO** [docs](.debian/README.md))

Some additional roles we'd like to support in the future:

- `nameserver`: setup primary/secondary DNS server
- `firewall`: let services declare their firewall policy
- `backup`: let services declare their backup procedures

# Community

We'd like to become a friendly and welcoming community. If you think the concepts here are cool, feel free to drop by on our XMPP chatroom [sysadmin@joinjabber.org](xmpp:sysadmin@joinjabber.org?join). If you don't have an XMPP account/client configured yet, you can use our webchat (requires JavaScript) on [anon.joinjabber.org](https://anon.joinjabber.org/) (Tor Browser users are more than welcome).

# Security

ansible-selfhosted, for lack of a better name, is alpha-quality software and bugs are expected. If we are doing something wrong and you would like to report it to us, please head over to our community chatroom as described above. We usually operate in the open, but if need be we can establish a more private and secure chat from there using OMEMO encryption (Signal protocol over XMPP).

# Copyleft (license)

This project is protected by [AGPLv3 license](LICENSE). We believe the fruit of human labor should belong to humanity as a whole, and privatization of resources and knowledge is harmful. If you don't have time to read the full license, you may:

- redistribute this project with or without further modification, under the same license
- use this project to serve your personal needs, without restriction (if the user-facing services are used by anyone else but you, it is not considered personal usage and falls under the next category)
- use this project to help/serve other persons, under the condition that you share every modification publicly under the same license
