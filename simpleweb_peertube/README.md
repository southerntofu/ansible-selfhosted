# simpleweb_peertube

This role installs a [simpleertube](https://git.sr.ht/~metalune/simpleweb_peertube) instance, to access Peertube from browsers who don't have JavaScript enabled. Features include:

- [x] Access Peertube without JavaScript
- [x] Subtitles support
- [ ] Configurable search backend (sepiasearch instance, currently only sepiasearch.org)
- [ ] No data is sent to remote Peertube instance before you start watching a video when NoScript is enabled (TODO: proxy images)
- [ ] Support multiple vhosts

## Settings

The following settings are supported, in `simpleweb_peertube.settings`:

- **required** `host`: the hostname to serve the simpleertube instance
- `accounts` (default: `[]`): a list of accounts whose latest videos to display on homepage, in the form of `user@server`, `@user@server` or `http(s)://server/a/user`
- `channels:` (default: `[]`): a list of channels whose latest videos to display on homepage, in the form of `channel@server`, `@channel@server`, or `http(s)://server/c/channel`
