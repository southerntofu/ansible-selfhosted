# mucbridge

This role configures a [cheogram-muc-bridge](https://git.singpolyma.net/cheogram-muc-bridge) component for `jabberserver`, so that it can relay messages across different MUC chatroom servers while having users appear on both sides, and supporting private messages as well (which `chatbridge` does not). In particular, this role intends to work with MUC rooms exposed by [biboumi IRC gateway](https://biboumi.louiz.org/), which can be installed via the [jabberserver role](../jabberserver/README.md).

**Note:** cheogram-muc-bridge is alpha software. Bugs can happen.

## Arguments

This role will configure some chans, defined in `mucbridge.chans` variable. Each entry can follow one of two formats: a low-level format close to cheogram-muc-bridge config, or a high-level format leveraging server settings defined in `mucbridge.settings.accounts` as explained in the [Settings](#settings) section.

Each entry of the low-level format is a list containing a certain number of room mappings, which each have the following settings:

- **(required)** `jid`: the address to reach the MUC chatroom
- **(required)** `tag`: a suffix (usually wrapped in `[]`, `__` otherwise) to apply to users from this chatroom, on other bridged chatrooms
- **(required)** `nickChars`: criteria for accepting characters as part of nicknames on this chatroom, should usually be `Some \"a-z[]A-Z\"` for IRC/biboumi side, `None Text` for regular MUC chatrooms
- **(required)** `nickLength`: the maximum nickname length for brided users on that network, can be either `Some INTEGER` (eg. `Some 32`) or `None Natural`

For example:

```
  chans:
    - [ { jid: "#foo%irc.net@biboumi", tag: "irc", nickChars: "Some \"a-zA-Z0-9`|^_{}[]\\\\-\"", nickLength: "Some 32"}, { jid: "foo@muc.com", tag: "xmpp", nickChars: "None Text", nickLength: "None Natural" } ]
```

Each entry of the high-level format is a mapping from servers (configured in `mucbridge.settings.accounts`) to a corresponding chatroom name. Assuming the `ircnet` and `muccom` servers from the examples above have been configured, the above snippet could be rewritten like so:

```
  chans:
    - { ircnet: "#foo", muccom: "foo" }
```

## Settings

The `mucbridge` role settings are defined in the configuration file, under the `mucbridge.settings` key. The following settings are available:

- **(required)** `host`: the domain name for use by the bridge
- `nick` (default: `bridge`): the bridge's nick across all chatrooms (**note:** if the nick is already taken on IRC side, connection will fail silently, see [upstream ticket](https://lab.louiz.org/louiz/biboumi/-/issues/3458))
- `gateway_irc` (default: `irc.localhost`): the default biboumi IRC gateway to use, unless overriden in a chan declaration; when unspecified or set to `irc.localhost` explicitly, a local biboumi instance is setup just for the bridge (and shut off from other components/hosts)
- `accounts`: a mapping of account names to account settings; account settings can have the following parameters
  - **(required)** `host`: the corresponding hostname to reach that chat server
  - `type` (default: `xmpp`): if type is `irc`, `gateway_irc` setting is appended to the corresponding chatroom names, and default `nickChars` and `nickLength` settings vary; also affects `tag` settings when type is `xmpp`
  - `tag`: the suffix (wrapped in `[]`) to apply to nicks coming from this network; when type is `xmpp`, an additional hash of the room's address is added to the tag, in order to prevent impersonation attacks (`tag: "jj"` will produce eg. `[jj-AbCdE]` suffixes on IRC side)
  - `nickChars`: the list of allowed characters in nicknames on that server; default to ``Some \"a-zA-Z0-9`|^_{}[]\\\\-\"`` when type is `irc`, `None Text` otherwise; forbidden characters are automatically replaced with `_`
  - `nickLength` (default: `32`): the maximum number of characters allowed in nicks on that server

Example:

```
  settings:
    accounts:
      irc:
        host: "irc.example.com"
        type: "irc"
        tag: "irc"
      muc:
        host: "muc.example.net"
        tag: "xmpp"
```

**Note that passwords are not stored in the configuration file (which is meant to be shared), as explained in the [Conventions section](#conventions)**

## Conventions

In order to interface with other roles/services, this role follows a certain number of conventions:

- passwords are stored plaintext in `/etc/prosody/secrets/` folder, in a file named after the bridge's host

## TODO

- [ ] In muc-bridge.dhall template, figure out how to write the comma `,` on the end of line for account profiles, for more consistent output; or maybe write it on start of line for chans declarations?
- [ ] Figure out how to support more characters on IRC side depending on remote charset
- [ ] How to support multiple biboumi instances when a local (eg. public) one is already configured?
- [ ] How to support multiple cheogram-muc-bridge instances?
- [ ] How to identify the bot with NickServ on IRC side to give it voice, maybe also give it privileges so it can voice the puppets? currently the bridge only works when channels are entirely public and don't require registration, which is good or bad depending on how exposed you are to spam/harassment
- [ ] How to give the bridge bot access to JIDs on trusted MUCs (in order to prevent impersonation, without giving it admin rights (not role/affiliation for that) ? Alternatively or in complement, how to enforce on trusted MUC side that nicks are global and not local to a MUC, which would enable to remove the tag hashes on that MUC network because nick collisions would be prevented
- [ ] When configured as a top-level role and a local biboumi gateway is configured, restrict s2s from biboumi to the cheogram-muc-bridge component specifically (using `mod_isolate_host`); otherwise, when set as part of a `primary` or `muc` jabberserver profile, restrict s2s from biboumi to local components only (`modules_disabled = { "s2s" }`)
