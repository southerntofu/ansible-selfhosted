{
	nick = "{{ mucbridge.settings.nick|default("bridge") }}",
	componentJid = "{{ mucbridge.settings.host }}",
	secret = "{{ lookup('password', '/etc/prosody/secrets/' ~ mucbridge.settings.host ~ ' length=64 chars=ascii_lowercase') }}",
	server = {
		host = "localhost",
		port = 5347
	},
	db = "/opt/mucbridge/muc-bridge.sqlite",
	mucs = [{% for bridge in mucbridge.chans %}

		[{% if bridge is mapping %}{% for entry in bridge|dict2items %}{% set serv = entry.key %}{% set chan = entry.value %}{% set srv = mucbridge.settings.accounts.get(serv) %}{% if srv.type|default("xmpp") == "irc" %}{% set remote = chan ~ "%" ~ srv.host ~ "@" ~ mucbridge.settings.get("gateway_irc", "irc.localhost") %}{% set tag = srv.tag %}{% else %}{% set remote = chan ~ "@" ~ srv.host %}{% set tag = srv.tag ~ '-' ~ remote | hash('md5') | b64encode | truncate(5, True, '') %}{% endif %}{# Entry is a { server: chan, ... } mapping #}

			{ jid = "{{ remote }}", tag = "{{ tag }}", nickChars = {{ srv.get('nickChars', 'None Text') }}, nickLength = Some {{ srv.get('nickLength', "32") }} }{% if not loop.last %},{% endif %}
{% endfor %}{% else %}{% for chan in bridge %}{# Entry is a [ { jid = "" ...}, ... ] list #}

			{ jid = "{{ chan.jid }}", tag = "{{ chan.tag }}", nickChars = {{ chan.nickChars }}, nickLength = {{ chan.nickLength }} }{% if not loop.last %},{% endif %}
{% endfor %}{% endif %}

		]{% if not loop.last %},{% endif %}

{% endfor %}{# Profiles #}{% for entry in mucbridge|dict2items() %}{% if entry.key != "chans" and entry.key != "settings" %}{% set firstprofile = "firstprofile" if firstprofile|default(true) == true else false %}{% for chan in entry.value %}

		{# Don't forget to add a comma if mucbridge.chans had entries #}{% if firstprofile and mucbridge.chans|default([])|length != 0 %},{% set firstprofile = false %}{% endif %}[{% for profile_entry in mucbridge.settings.profiles.get(entry.key)|dict2items() %}{% set serv = profile_entry.key %}{% set chan = profile_entry.value|replace('$room', chan) %}{% set srv = mucbridge.settings.accounts.get(serv) %}{% if srv.type|default("xmpp") == "irc" %}{% set remote = chan ~ "%" ~ srv.host ~ "@" ~ mucbridge.settings.get("gateway_irc", "irc.localhost") %}{% set tag = srv.tag %}{% else %}{% set remote = chan ~ "@" ~ srv.host %}{% set tag = srv.tag ~ '-' ~ remote | hash('md5') | b64encode | truncate(5, True, '') %}{% endif %}


			{ jid = "{{ remote }}", tag = "{{ tag }}", nickChars = {{ srv.get('nickChars', 'Some "a-zA-Z0-9`|^_{}[]\\\\-"' if srv.type|default('xmpp') == 'irc' else 'None Text') }}, nickLength = Some {{ srv.get('nickLength', "32") }} }{% if not loop.last %},{% endif %}
		{% endfor %}

		]{# TODO: repair firstprofile detection and re-place a comma here #}
{% endfor %}{% endif %}{% endfor %}

	]
}
